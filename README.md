# Progress Exemplo Anexos MLA

Exemplo de programa para criar anexos em aprovacoes de solicitação de compras do MLA.

Pré-requisitos para testar esta customização:

* Totvs Datasul versão 12 instalado (base de testes);
* Datasul configurado para aprovação de solicitação de compras no MLA;
* Aprovador Conector, para configuração da integração com o Aprovador;

Acesse o [Portal do Desenvolvedor Aprovador](https://aprovador.com/documentacao/) para saber mais sobre as customizações Progress através do Aprovador Conector.

# Aprovador

Para saber mais sobre o Aprovador, acesse [https://aprovador.com](https://aprovador.com). Para saber como customizar o Aprovador, acesse o [Portal do Desenvolvedor](https://aprovador.com/documentacao/).

# Licença

Os fontes disponíveis neste repositório são licenciados sob a [Licença MIT](https://opensource.org/licenses/MIT).

---

_Totvs Datasul é uma marca registrada da Totvs S.A_.